package main

import (
	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
	"gitlab.digilol.net/moneropay/metronero-poc/internal/server"
)

func main() {
	daemon.Run()
	server.Run()
}
