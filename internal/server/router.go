package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/jwtauth/v5"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"

	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
	"gitlab.digilol.net/moneropay/metronero-poc/internal/server/controller"
)

func initRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middlewareServerHeader)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	fs := http.FileServer(http.Dir("assets"))
	r.Handle("/assets/*", http.StripPrefix("/assets/", fs))
	r.Get("/", controller.HomepageHandler)
	r.Get("/health", controller.HealthHandler)
	r.Route("/account", func(r chi.Router) {
		r.Post("/login", controller.LoginHandler)
		r.Get("/login", controller.LoginPageHandler)
		r.Post("/register", controller.RegisterHandler)
		r.Get("/register", controller.RegisterPageHandler)
		r.Group(func(r chi.Router) {
			r.Use(jwtauth.Verify(daemon.Config.JwtSecret, jwtauth.TokenFromCookie))
			r.Use(jwtauth.Authenticator)
			r.Post("/template", controller.TemplateHandler)
			r.Post("/payment", controller.InitiatePaymentHandler)
			r.Get("/create_payment", controller.CreatePaymentPageHandler)
			r.Post("/create_payment", controller.InitiatePaymentHandler)
			r.Post("/withdraw", controller.WithdrawalHandler)
			r.Get("/", controller.MerchantPanelHandler)
		})
	})
	r.Get("/p/{uuid}", controller.PaymentGetHandler)
	r.Post("/callback/{uuid}", controller.CallbackHandler)
	return r
}

func Run() {
	h2s := &http2.Server{}
	srv := &http.Server{
		Addr: daemon.Config.BindAddr,
		Handler: h2c.NewHandler(initRouter(), h2s),
	}
	serverCtx, serverStopCtx := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sig
		shutdownCtx, _ := context.WithTimeout(serverCtx, 30 * time.Second)
		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("Graceful shutdown timed out. Forcing exit.")
			}
		}()
		err := srv.Shutdown(shutdownCtx)
		if err != nil {
			log.Fatal(err)
		}
		serverStopCtx()
	}()
	err := srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
	<-serverCtx.Done()
}
