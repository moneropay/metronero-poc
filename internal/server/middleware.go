package server

import (
	"net/http"

	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
)

func middlewareServerHeader(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Server", "Metronero/" + daemon.Version)
		next.ServeHTTP(w, r)
	})
}
