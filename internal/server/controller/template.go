package controller

import (
	"net/http"
	"io/ioutil"
	//"log"

	"github.com/go-chi/jwtauth/v5"

	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
)

func TemplateHandler(w http.ResponseWriter, r *http.Request) {
	_, c, err := jwtauth.FromContext(r.Context())
	f, _, err := r.FormFile("file")
	if err != nil {
		writeError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer f.Close()
	b, err := ioutil.ReadAll(f)
	if err != nil {
		writeError(w, http.StatusBadRequest, err.Error())
		return
	}
	if err := daemon.TemplateUpdate(r.Context(), c["username"].(string), string(b)); err != nil {
		writeError(w, http.StatusBadRequest, err.Error())
		return
	}
	http.Redirect(w, r, "/account", http.StatusSeeOther)
}

func MerchantPanelHandler(w http.ResponseWriter, r *http.Request) {
	_, c, err := jwtauth.FromContext(r.Context())
	username := c["username"].(string)
	p, err := daemon.PaymentsByMerchant(r.Context(), username)
	if err != nil {
		writeError(w, http.StatusBadRequest, err.Error())
		return
	}
	items := struct {
		Username string
		Payments []daemon.Payment
	}{
		Username: username,
		Payments: p,
	}
	if err := daemon.MerchantPanel.Execute(w, items); err != nil {
		writeError(w, http.StatusBadRequest, err.Error())
		return
	}
}

func HomepageHandler(w http.ResponseWriter, r *http.Request) {
	if err := daemon.Homepage.Execute(w, nil); err != nil {
		writeError(w, http.StatusInternalServerError, err.Error())
	}
}
