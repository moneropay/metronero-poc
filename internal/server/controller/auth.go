package controller

import (
	"net/http"
	//"time"

	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
)

func authAccount(w http.ResponseWriter, r *http.Request, u string) {
	t, err := daemon.CreateSecret(u)
	if err != nil {
		writeError(w, http.StatusInternalServerError, "Failed to issue token")
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name: "jwt",
		Value: t,
		Path: "/account",
		//Expires: time.Now().Add(24 * time.Hour),
		Secure: true,
		HttpOnly: true,
	})
	http.Redirect(w, r, "/account", http.StatusSeeOther)
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")
	if username == "" || password == "" {
		writeError(w, http.StatusBadRequest, "Required field(s) cannot be empty")
		return
	}
	if err := daemon.UserLogin(r.Context(), username, password); err != nil {
		writeError(w, http.StatusOK, "Wrong username or password")
		return
	}
	authAccount(w, r, username)
}

func LoginPageHandler(w http.ResponseWriter, r *http.Request) {
	if err := daemon.LoginPage.Execute(w, nil); err != nil {
		writeError(w, http.StatusInternalServerError, err.Error())
	}
}

// On successful registration returns 200 with the text OK
func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")
	walletAddress := r.FormValue("wallet_address")
	if username == "" || password == "" || walletAddress == "" {
		writeError(w, http.StatusBadRequest, "Required field(s) cannot be empty")
		return
	}
	// TODO: Better error checking here, preferably using pgx family lib
	if err := daemon.UserRegister(r.Context(), username, password, walletAddress); err != nil {
		writeError(w, http.StatusInternalServerError, err.Error())
		return
	}
	authAccount(w, r, username)
}

func RegisterPageHandler(w http.ResponseWriter, r *http.Request) {
	if err := daemon.RegisterPage.Execute(w, nil); err != nil {
		writeError(w, http.StatusInternalServerError, err.Error())
	}
}
