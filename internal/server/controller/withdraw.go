package controller

import (
	"net/http"

	"github.com/go-chi/jwtauth/v5"
	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
)

func WithdrawalHandler(w http.ResponseWriter, r *http.Request) {
	_, c, err := jwtauth.FromContext(r.Context())
	if err != nil {
		writeError(w, http.StatusBadRequest, err.Error())
		return
	}
	ctx := r.Context()
	if err := daemon.DoWithdrawals(ctx, c["username"].(string)); err != nil {
		writeError(w, http.StatusInternalServerError, err.Error())
	}
}
