package controller

import (
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"gitlab.com/moneropay/go-monero/walletrpc"

	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
)

func InitiatePaymentHandler(w http.ResponseWriter, r *http.Request) {
	_, c, err := jwtauth.FromContext(r.Context())
	if err != nil {
		writeError(w, http.StatusBadRequest, err.Error())
		return
	}
	amount, err := strconv.ParseUint(r.FormValue("amount"), 10, 64)
	if err != nil {
		writeError(w, http.StatusBadRequest, "Failed to parse the amount")
		return
	}
	cancelUrl := r.FormValue("cancelUrl")
	acceptUrl := r.FormValue("acceptUrl")
	callbackUrl := r.FormValue("callbackUrl")
	if amount == 0 || cancelUrl == "" || acceptUrl == "" || callbackUrl == "" {
		writeError(w, http.StatusBadRequest, "Missing required parameter")
		return
	}
	uuid, err := daemon.PaymentCreateProcess(r.Context(), c["username"].(string), amount, cancelUrl, acceptUrl, callbackUrl)
	if err != nil {
		writeError(w, http.StatusInternalServerError, "Failed to initiate a payment")
		log.Println(err)
		return
	}
	http.Redirect(w, r, "/p/" + uuid, http.StatusSeeOther)
}

func PaymentGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	paymentId := chi.URLParam(r, "uuid")
	template, amount, acceptUrl, cancelUrl, received, completed, address, err := daemon.PaymentPageDetails(ctx, paymentId)
	if err != nil {
		writeError(w, http.StatusInternalServerError, "Failed to render the merchants template")
		log.Println(err)
		return
	}
	if completed {
		w.Write([]byte("Payment already completed"))
		return
	}
	items := struct {
		Amount string
		AmountXMR, Address, QrUrl, CancelUrl string
	}{
		Amount: strings.TrimRight(walletrpc.XMRToDecimal(amount), "0"),
		Address: address,
		QrUrl: "https://ksvgcode.digilol.net/qr?content=monero%3A" + address + "%3Ftx_amount%3D" + walletrpc.XMRToDecimal(amount),
		CancelUrl: cancelUrl,
	}
	if received {
		if err := daemon.PaymentComplete(ctx, paymentId); err != nil {
			writeError(w, http.StatusInternalServerError, "Failed to set payment complete")
			log.Println(err)
			return
		}
		http.Redirect(w, r, acceptUrl, http.StatusSeeOther)
		return
	}
	w.Header().Set("Refresh", "10") // TODO: Make this customiozable or turnable off, so people can handle this via meta tag or JS instead.
	template.Execute(w, items)
}

func CreatePaymentPageHandler(w http.ResponseWriter, r *http.Request) {
	if err := daemon.CreatePaymentPage.Execute(w, nil); err != nil {
		writeError(w, http.StatusInternalServerError, err.Error())
	}
}
