package controller

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"

	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
)

type TransactionData struct {
	Amount uint64 `json:"amount"`
	Confirmations uint64 `json:"confirmations"`
	DoubleSpendSeen bool `json:"double_spend_seen"`
	Fee uint64 `json:"fee"`
	Height uint64 `json:"height"`
	Timestamp time.Time `json:"timestamp"`
	TxHash string `json:"tx_hash"`
	UnlockTime uint64 `json:"unlock_time"`
	Locked bool `json:"locked"`
}

type callbackData struct {
	Amount struct {
		Expected uint64 `json:"expected"`
		Covered struct {
			Total uint64 `json:"total"`
			Unlocked uint64 `json:"unlocked"`
		} `json:"covered"`
	} `json:"amount"`
	Complete bool `json:"complete"`
	Description string `json:"description,omitempty"`
	CreatedAt time.Time `json:"created_at"`
	Transaction TransactionData `json:"transaction"`
}

func CallbackHandler(w http.ResponseWriter, r *http.Request) {
	paymentId := chi.URLParam(r, "uuid")
	b, err := io.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Println(err)
		return
	}
	var data callbackData
	if err := json.Unmarshal(b, &data); err != nil {
		log.Println(err)
		return
	}
	if data.Transaction.Locked && data.Amount.Covered.Total >= data.Amount.Expected {
		if err := daemon.PaymentReceived(r.Context(), paymentId); err != nil {
			log.Println(err)
			return
		}
	}
	callbackUrl, err := daemon.PaymentCallbackUrl(r.Context(), paymentId)
	if err != nil {
		log.Println(err)
		return
	}
	if _, err := http.Post(callbackUrl, "application/json", bytes.NewBuffer(b)); err != nil {
		log.Println(err)
	}
}