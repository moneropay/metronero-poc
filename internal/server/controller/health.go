package controller

import (
	"encoding/json"
	"net/http"

	"gitlab.digilol.net/moneropay/metronero-poc/internal/daemon"
)

func HealthHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	resp := daemon.Health(r.Context())
	w.WriteHeader(resp.Status)
	json.NewEncoder(w).Encode(resp)
}
