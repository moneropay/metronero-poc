package daemon

const Version = "0.0.0"

func Run() {
	loadConfig()
	pdbConnect()
	pdbMigrate()
	prerenderTemplates()
}
