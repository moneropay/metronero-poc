package daemon

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"time"
	"strings"

	"github.com/gofrs/uuid"
	"gitlab.com/moneropay/go-monero/walletrpc"
)

type subaddressRequest struct {
	Amount uint64 `json:"amount"`
	Description string `json:"description"`
	CallbackUrl string `json:"callback_url"`
}

type subaddressResponse struct {
	Address string `json:"address"`
}

func paymentNewAddress(ctx context.Context, amount uint64, uuid string) (string, error) {
	jr := subaddressRequest{
		Amount: amount,
		Description: "Metronero/" + Version,
		CallbackUrl: Config.callbackURL + "/" + uuid,
	}
	b := new(bytes.Buffer)
	if err := json.NewEncoder(b).Encode(jr); err != nil {
		return "", err
	}
	req, err := http.NewRequest("POST", Config.moneropay + "/receive", b)
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json")
	cl := &http.Client{Timeout: 15 * time.Second}
	resp, err := cl.Do(req)
	if err != nil {
		return "", err
	}
	var mr subaddressResponse
	err = json.NewDecoder(resp.Body).Decode(&mr)
	return mr.Address, err
}

func PaymentCreateProcess(ctx context.Context, username string, amount uint64, cancelUrl, acceptUrl, callbackUrl string) (string, error) {
	uuidStr := uuid.Must(uuid.NewV4()).String()
	address, err := paymentNewAddress(ctx, amount, uuidStr)
	if err != nil {
		return "", err
	}
	err = pdbExec(ctx, "INSERT INTO payments(id,amount,accept_url,cancel_url,callback_url,username,address)VALUES($1,$2,$3,$4,$5,$6,$7)",
	    uuidStr, amount, acceptUrl, cancelUrl, callbackUrl, username, address)
	return uuidStr, err
}

func PaymentReceived(ctx context.Context, paymentId string) error {
	return pdbExec(ctx, "UPDATE payments SET received=true WHERE id=$1", paymentId)
}

func PaymentComplete(ctx context.Context, paymentId string) error {
	return pdbExec(ctx, "UPDATE payments SET completed=true WHERE id=$1", paymentId)
}

func PaymentCallbackUrl(ctx context.Context, paymentId string) (string, error) {
	row, err := pdbQueryRow(ctx, "SELECT callback_url FROM payments WHERE id=$1", paymentId)
	if err != nil {
		return "", err
	}
	var callbackUrl string
	err = row.Scan(&callbackUrl)
	return callbackUrl, err
}

type Payment struct {
	Uuid string
	Amount string
	Complete bool
}

func PaymentsByMerchant(ctx context.Context, username string) ([]Payment, error) {
	var payments []Payment
	rows, err := pdbQuery(ctx, "SELECT id,amount,completed FROM payments WHERE username=$1 LIMIT 5", username)
	if err != nil {
		return payments, err
	}
	for rows.Next() {
		var p Payment
		var amount uint64
		if err := rows.Scan(&p.Uuid, &amount, &p.Complete); err != nil {
			return payments, err
		}
		p.Amount = strings.TrimRight(walletrpc.XMRToDecimal(amount), "0")
		payments = append(payments, p)
	}
	return payments, nil
}
