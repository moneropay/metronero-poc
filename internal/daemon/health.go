package daemon

import (
	"context"
	"net/http"
	"time"
)

type HealthStatus struct {
	Status int `json:"status"`
	Services struct {
		PostgreSQL bool `json:"postgresql"`
		MoneroPay bool `json:"moneropay"`
	} `json:"services"`
}

func Health(ctx context.Context) (HealthStatus) {
	d := HealthStatus{Status: http.StatusOK}
	ctx, c1 := context.WithTimeout(context.Background(), 10 * time.Second)
	defer c1()
	if err := pdb.Ping(ctx); err == nil {
		d.Services.PostgreSQL = true
	}
	ctx, c2 := context.WithTimeout(context.Background(), 10 * time.Second)
	defer c2()
	d.Services.MoneroPay = true // TODO: Implement actual health check for MoneroPay
	if !d.Services.PostgreSQL || !d.Services.MoneroPay {
		d.Status = http.StatusServiceUnavailable
	}
	return d
}

