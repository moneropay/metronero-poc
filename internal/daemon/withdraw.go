package daemon

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/moneropay/go-monero/walletrpc"
)

type transferPostRequest struct {
	Destinations []walletrpc.Destination `json:"destinations"`
}

func DoWithdrawals(ctx context.Context, username string) error {
	w, ids, err := getWithdrawals(ctx, username)
	if err != nil {
		return err
	}
	if len(w) == 0 {
		return nil
	}
	jr := &transferPostRequest{Destinations: w}
	b := new(bytes.Buffer)
	if err := json.NewEncoder(b).Encode(jr); err != nil {
		return err
	}
	req, err := http.NewRequest("POST", Config.moneropay, b)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	cl := &http.Client{Timeout: 15 * time.Second}
	// TODO: Handle the response here for better error checking
	_, err = cl.Do(req)
	if err != nil {
		return err
	}
	return setWithdrawnTrue(ctx, ids)
}

func getWithdrawals(ctx context.Context, username string) (w []walletrpc.Destination, ids []string, err error) {
	row, err := pdbQueryRow(ctx, "SELECT commission,wallet_address FROM accounts WHERE username=$1", username)
	if err != nil {
		return nil, nil, err
	}
	var c int
	var a string
	if err = row.Scan(&c, &a); err != nil {
		return nil, nil, err
	}
	rows, err := pdbQuery(ctx,
	"SELECT id,amount FROM payments WHERE username=$1 AND completed=true AND withdrawn=false",
	    username)
	if err != nil {
		return nil, nil, err
	}
	for rows.Next() {
		var t walletrpc.Destination
		var i string
		if err = rows.Scan(&i, &t.Amount); err != nil {
			return nil, nil, err
		}
		t.Address = a
		w = append(w, t)
		ids = append(ids, i)
	}
	return
}

func setWithdrawnTrue(ctx context.Context, ids []string) error {
	for i, _ := range ids {
		if err := pdbExec(ctx, "UPDATE payments SET withdrawn=true WHERE id=$1", i); err != nil {
			return err
		}
	}
	return nil
}
