package daemon

import (
	"context"
	"html/template"
	"log"
	"os"
)

var Homepage *template.Template
var RegisterPage *template.Template
var LoginPage *template.Template
var MerchantPanel *template.Template
var CreatePaymentPage *template.Template
var SuccessPage *template.Template
var FailPage *template.Template
var defaultTemplate string

func prerenderTemplates() {
	var err error
	Homepage, err = template.ParseFiles("template/index.html")
	if err != nil {
		log.Fatal(err)
	}
	RegisterPage, err = template.ParseFiles("template/register.html")
	if err != nil {
		log.Fatal(err)
	}
	LoginPage, err = template.ParseFiles("template/login.html")
	if err != nil {
		log.Fatal(err)
	}
	MerchantPanel, err = template.ParseFiles("template/merchant-panel.html")
	if err != nil {
		log.Fatal(err)
	}
	CreatePaymentPage, err = template.ParseFiles("template/create-payment.html")
	if err != nil {
		log.Fatal(err)
	}
	SuccessPage, err = template.ParseFiles("template/success.html")
	if err != nil {
		log.Fatal(err)
	}
	FailPage, err = template.ParseFiles("template/fail.html")
	if err != nil {
		log.Fatal(err)
	}
	dtBytes, err := os.ReadFile("assets/default.html")
	if err != nil {
		log.Fatal(err)
	}
	defaultTemplate = string(dtBytes)
}

func TemplateUpdate(ctx context.Context, username, templateStr string) error {
	return pdbExec(ctx, "UPDATE accounts SET template=$1 WHERE username=$2", templateStr, username)
}

func TemplateFetch(ctx context.Context, username string) (templateStr string, err error) {
	row, err := pdbQueryRow(ctx, "SELECT template FROM accounts WHERE username=$1", username)
	if err != nil {
		return
	}
	err = row.Scan(&templateStr)
	return
}

func PaymentPageDetails(ctx context.Context, paymentId string) (t *template.Template, amount uint64,
	acceptUrl, cancelUrl string, received, completed bool, address string, err error) {
	row, err := pdbQueryRow(ctx,
	    "SELECT template,amount,accept_url,cancel_url,received,completed,address" +
	    " FROM accounts,payments WHERE accounts.username=payments.username AND payments.id=$1", paymentId)
	if err != nil {
		return
	}
	var templateText string
	if err = row.Scan(&templateText, &amount, &acceptUrl, &cancelUrl, &received, &completed, &address); err != nil {
		return
	}
	if completed {
		t = nil
		return
	}
	t, err = template.New("t").Parse(templateText)
	return
}
